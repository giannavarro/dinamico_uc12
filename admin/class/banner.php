<?php
class Banners{
    public $id;
    public $titulo;
    public $link;
    public $imagem;
    public $alt;
    public $ativo;

    public static function loadById($id_banner){
        $sql = new SQL();
        $resultado = $sql->select("select * from banner where id = :id", array(":id"=>$id_banner));
        if (count($resultado)>0) {
            return $resultado[0];
        }
    }

    public static function getList(){
        $sql = new SQL();
        return $sql->select("select * from banner order by titulo_banner");
    }

    public static function search($banner){
        $sql = new SQL();
        return $sql->select("select * from banner where nome :titulo_banner", array(":titulo_banner"=>"%".$banner."%"));
    }

    public static function setData($data){
        $id = ($data['id_banner']);
        $titulo = ($data['titulo_banner']);
        $link = ($data['link_banner']);
        $imagem = ($data['img_banner']);
        $alt = ($data['alt']);
        $ativo = ($data['banner_ativo']);
    }

    public function insert(){
        $sql = new SQL();
        $resultado = $sql->select("call sp_banner_insert(:titulo_banner,:link_banner,:img_banner,:alt,:banner_ativo)", 
        array(
            ":titulo_banner"=>$this->titulo,
            ":link_banner"=>$this->link,
            ":img_banner"=>$this->imagem,
            ":alt"=>$this->alt,
            ":banner_ativo"=>$this->ativo
        ));
        if (count($resultado) > 0) {
            return $resultado[0];
        }
    }

    public function delete(){
        $sql = new SQL();
        $sql->query("DELETE FROM banner WHERE id = :id",array(
            ":id"=>$this->id
        ));
    }
    
    public function update($_id,$_titulo,$_link,$_imagem,$_alt,$_ativo){
        $sql = new SQL();
        $sql->query("UPDATE banner SET titulo_banner = :titulo_banner, link = :link, img_banner = :imagem, alt = :alt, banner_ativo = :ativo WHERE id = :id", 
        array(
            ":id"=>$_id,
            ":titulo_banner"=>$_titulo,
            ":link"=>$_link,
            ":imagem"=>$_imagem,
            ":alt"=>$_alt,
            ":ativo"=>$_ativo
        ));
    }

    public function __construct($_titulo="",$_link ="",$_imagem ="",$_alt ="",$_ativo =""){
        $this->titulo=$_titulo;
        $this->link=$_link;
        $this->imagem=$_imagem;
        $this->alt=$_alt;
        $this->ativo=$_ativo;
    }

    public function __toString(){

        return json_encode(array(
            ":id"=>$this->id,
            ":titulo"=>$this->titulo,
            ":link"=>$this->link,
            ":imagem"=>$this->imagem,
            ":alt"=>$this->alt,
            ":ativo"=>$this->ativo
        ));
    }
}
?>