<?php
class Sql extends PDO{
    private $cn;
    public function __construct(){
        $this->cn = new PDO("mysql:host=localhost;dbname=dinamicodb","root","");
    }
    private function setParams($comando, $paramentros=array()){
        foreach ($paramentros as $key => $value){
            $this->setParam($comando,$key,$value);
        }
    }
    public function setParam($comando, $key, $value){
        $comando->bindParam($key,$value);
    }
    public function query($comandoSql, $params = array()){
        $cmd = $this->cn->prepare($comandoSql);
        $this->setParams($cmd, $params);
        $cmd->execute();
        return $cmd;
    }
    public function select($comandoSql,$params = array()){
        $cmd = $this->query($comandoSql, $params);
        return $cmd->fetchAll(PDO::FETCH_ASSOC);
    }

}

//     DELIMITER $$
        // CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_post_insert`(
        // _id_post int(11),
        // _id_categoria int(11),
        // _titulo_post varchar(250),
        // _descricao_post text,
        // _visitas int(11),
        // _data_post date,
        // post_ativo varchar(1)
        // )
        // BEGIN
        // 	insert into post (id_post, id_categoria, titulo_post, descricao_post, img_post, visitas, data_post, post_ativo)
        //     values (_id_post, _id_categoria, _titulo_post, _descricao_post ,_visitas, _data_post,post_ativo);
        //     select * from post where id = (select @@identity);    
        // END




        // delimiter $$
    // CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_categoria_insert`(
    //     _categoria varchar(150), 
    //     _cat_ativo varchar(1)
    //     )
    //     BEGIN
    //         insert into categoria (categoria, cat_ativo)
    //         values (_categoria, _cat_ativo);
    //         select * from categoria where id_categoria = (select @@identity);    
    //     END $$

   
   
   
    // PROCEDURE INSERT
        // Delimiter $$

        // create PROCEDURE sp_banner_insert
        // (
        // sptitulo varchar(255),
        // splink varchar(255),
        // spimg varchar(150),
        // spalt varchar(255),
        // spativo varchar(1))

        // Begin
        // insert into banner(titulo_banner,link_banner,img_banner,alt,banner_ativo) values(
        // sptitulo,
        // splink,
        // spimg,
        // spalt,
        // spativo);
        // select * from banner where id_banner = (select @@identity);
        // END$$
?>
